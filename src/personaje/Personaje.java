package personaje;

import java.util.ArrayList;
import java.util.List;

import animales.Animal;
import pocimas.Pocima;
import tool.Tool;
import varita.Varita;
import varita.VaritaFalsa;

public class Personaje {
	
	private int sabiduria = 0;
	private List<Pocima> pocimas = new ArrayList<>();
	private List<Animal> animalesPropios = new ArrayList<>();
	private Varita varita = new VaritaFalsa(this);
	
	
	public void agregarPocima(Pocima pocima) {
		this.pocimas.add(pocima);
	}


	public List<Pocima> getPocimas() {
		return pocimas;
	}


	public int getSabiduria() {
		return sabiduria;
	}
	
	public void aumentarSabiduria(int aumentar){
		this.sabiduria = Tool.unico().noSubeDeTreinta(this.getSabiduria(), aumentar);
	}


	public void disminuirSabiduria(int disminuir) {
		this.sabiduria = Tool.unico().noBajarDeCero(this.sabiduria, disminuir);
	}


	public Pocima getPocimaAzul() {
		return this.getPocimas().stream().filter(p -> p.getNombre().equals("Azul")).findAny().get();
	}
	
	public Pocima getPocimaRoja() {
		return this.getPocimas().stream().filter(p -> p.getNombre().equals("Roja")).findAny().get();
	}

	public List<Animal> getAnimalesPropios() {
		return animalesPropios;
	}
	
	public void agregarAnimal(Animal animal){
		this.animalesPropios.add(animal);
		animal.setDuenio(this);
	}


	public boolean tienePocimaAzul() {
		return this.getPocimas().stream().anyMatch(p -> p.getNombre().equals("Azul"));
	}


	public boolean tienePocimaRoja() {
		return this.getPocimas().stream().anyMatch(p -> p.getNombre().equals("Roja"));
	}


	public Varita getVarita() {
		return varita;
	}


	public void nuevaVarita(Varita varita) {
		this.varita = varita;
	}
	
	public void usarVarita(Animal animal){
		this.varita.aplicarse(animal);
	}


	public boolean tienePocimaAmarilla() {
		return this.getPocimas().stream().anyMatch(p -> p.getNombre().equals("Amarilla"));
	}


	public Pocima getPocimaAmarilla() {
		return this.getPocimas().stream().filter(p -> p.getNombre().equals("Amarilla")).findAny().get();
	}
	
	private int getEnergiaDeAnimales(){
		return this.animalesPropios.stream().mapToInt(p -> p.getEnergia()).sum();
	}
	
	public int fortaleza(){
		return this.getSabiduria() + this.getEnergiaDeAnimales();
	}


	public void quitarPocima(Pocima pocima) {
		this.pocimas.remove(pocima);
	}
	
}
