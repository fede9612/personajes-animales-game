package animales;

import personaje.Personaje;

public class Hiena extends Animal{
	
	public Hiena(int resistencia, int energia) {
		this.setResistencia(resistencia);
		this.setEnergia(energia);
	}

	@Override
	public void evaluarPosibleCambioDeDuenio(Personaje per) {
		if(dejarDuenio()){
			this.getDuenio().getAnimalesPropios().remove(this);
			this.setDuenio(this.getNoDuenio()); 
		}else if(this.esLibre() && (this.getEnergiaTodaJunta() == 8)){
			this.setDuenio(per);
			per.agregarAnimal(this);
		}
	}

	private boolean dejarDuenio() {
		return (this.getEnergia() % 3) == 0;
	}
	
}
