package animales;

import personaje.Personaje;

public class Buey extends Animal{
	
	public Buey(int resistencia, int energia){
		this.setResistencia(resistencia);
		this.setEnergia(energia);
	}

	@Override
	public void evaluarPosibleCambioDeDuenio(Personaje per) {
		if(dejarDuenio()){
			this.getDuenio().getAnimalesPropios().remove(this);
			this.setDuenio(this.getNoDuenio()); 
		}else if(this.esLibre() && (this.getFuerzaVinculo() > 10)){
			this.setDuenio(per);
			per.agregarAnimal(this);
		}
	}

	private boolean dejarDuenio() {
		return this.getFuerzaVinculo() < 5;
	}
	
}
