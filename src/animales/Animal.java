package animales;

import personaje.Personaje;
import tool.Tool;

public abstract class Animal {
	
	private int energia;
	private Personaje noDuenio = new Personaje();
	private int resistencia;
	private Personaje duenio = noDuenio;
	private int fuerzaVinculo;
	
	//Este atributo lo usa la hiena
	private int energiaTodaJunta;
	
	public int getEnergia() {
		return energia;
	}
	
	public void setEnergia(int energia) {
		this.energia = energia;
	}
	
	public int getResistencia() {
		return resistencia;
	}
	
	public void setResistencia(int resistencia) {
		this.resistencia = resistencia;
	}
	
	public boolean esDe(Personaje per) {
		return duenio.equals(per);
	}
	
	public Personaje getDuenio() {
		return duenio;
	}
	
	public void setDuenio(Personaje duenio) {
		this.duenio = duenio;
	}

	public void subirResistencia(int subeResistencia) {
		this.resistencia += subeResistencia;
	}

	public void bajarResistencia(int bajaResistencia) {
		this.resistencia = Tool.unico().noBajarDeCero(this.resistencia, bajaResistencia);
	}

	public void subirEnergia(int aumentaEnergia) {
		if(aumentaEnergia == 8){
			this.setEnergiaTodaJunta(8);
		}
		this.energia += aumentaEnergia;
	}
	
	public void subirFuerzaVinculo(int aumentar){
		this.fuerzaVinculo += aumentar;
	}
	
	public int getFuerzaVinculo(){
		return this.fuerzaVinculo;
	}

	public void bajarEnergia(int bajaEnergiaAnimal) {
		this.energia = Tool.unico().noBajarDeCero(this.energia, bajaEnergiaAnimal);
	}

	public void bajaVinculo(int bajaVinculoConDueño) {
		this.fuerzaVinculo = Tool.unico().noBajarDeCero(this.getFuerzaVinculo(), bajaVinculoConDueño);
	}
	
	public abstract void evaluarPosibleCambioDeDuenio(Personaje per);

	public Personaje getNoDuenio() {
		return noDuenio;
	}
	
	public boolean esLibre(){
		return this.getDuenio().equals(getNoDuenio());
	}

	public int getEnergiaTodaJunta() {
		return energiaTodaJunta;
	}

	public void setEnergiaTodaJunta(int energiaTodaJunta) {
		this.energiaTodaJunta = energiaTodaJunta;
	}
}
