package animales;

public class DecoradorAnimal {

	private Animal animal;
	
	public DecoradorAnimal(Animal animal) {
		this.animal = animal;
	}
	
	public String getTipoDeAnimal(){
		return this.animal.getClass().getSimpleName();
	}
	
}
