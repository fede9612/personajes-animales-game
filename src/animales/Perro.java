package animales;

import personaje.Personaje;

public class Perro extends Animal{
	
	public Perro(int resistencia, int energia) {
		this.setResistencia(resistencia);
		this.setEnergia(energia);
	}

	@Override
	public void evaluarPosibleCambioDeDuenio(Personaje per) {
		if(dejarDuenio()){
			this.getDuenio().getAnimalesPropios().remove(this);
			this.getDuenio().disminuirSabiduria(1);
			this.setDuenio(this.getNoDuenio());
		}else if(this.esLibre() && (this.getFuerzaVinculo() >= 5)){
			this.setDuenio(per);
			per.agregarAnimal(this);
			per.aumentarSabiduria(1);
		}
	}

	private boolean dejarDuenio() {
		return this.getFuerzaVinculo() == 0;
	}
	
}
