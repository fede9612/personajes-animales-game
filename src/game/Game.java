package game;

import java.util.ArrayList;
import java.util.List;

import animales.Animal;
import castillos.Castillo;
import criterios.Criterio;
import criterios.Inicial;
import personaje.Personaje;

public class Game {
	
	private static Game game;
	private List<Animal> animalesSueltos = new ArrayList<>();
	private List<Castillo> castillos = new ArrayList<>();
	private Criterio criterio = new Inicial();
	
	private Game() {
		
	}
	
	public static Game unico(){
		if (game == null){
            game = new Game();
        }
		return game;
	}

	public void agregarAnimal(Animal animal){
		this.animalesSueltos.add(animal);
	}
	
	public void agregarCastillo(Castillo castillo){
		this.castillos.add(castillo);
	}
	
	public List<Animal> getAnimalesSueltos() {
		return animalesSueltos;
	}
	
	public void cambiarCriterio(Criterio criterio){
		this.criterio = criterio;
	}
	
	public void encuentro(Personaje per, Animal animal){
		if(animal.esDe(per)){
			this.criterio.aplicarseConDuenio(per, animal);
		}else{
			this.criterio.aplicarseSinDuenio(per, animal);
		}
	}
	
	public int antiguedad(){
		return this.castillos.stream().mapToInt(c -> c.getCantidadDeVisitantes()).sum();
	}

	public List<Castillo> getCastillos() {
		return castillos;
	}
}
