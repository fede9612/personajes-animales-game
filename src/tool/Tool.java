package tool;

public class Tool {

	private static Tool tool;
	
	private Tool(){
		
	}
	
	public static Tool unico(){
		if (tool == null){
            tool = new Tool();
        }
		return tool;
	}
	
	public int noBajarDeCero(int n1, int n2){
		if((n1 - n2) < 0){
			return 0;
		}else{
			return n1 - n2;
		}
	}
	
	public int noSubeDeTreinta(int n1, int n2){
		if((n1 + n2) >= 30){
			return 30;
		}else{
			return n1 + n2;
		}
	}
	
}
