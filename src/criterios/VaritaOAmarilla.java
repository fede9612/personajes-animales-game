package criterios;

import animales.Animal;
import personaje.Personaje;

public class VaritaOAmarilla extends Criterio{

	@Override
	public void aplicarseConDuenio(Personaje per, Animal animal) {
		per.usarVarita(animal);
	}

	@Override
	public void aplicarseSinDuenio(Personaje per, Animal animal) {
		if(per.tienePocimaAmarilla()){
			animal.bajarEnergia(per.getPocimaAmarilla().getBajaEnergiaAnimal());
			animal.bajaVinculo(per.getPocimaAmarilla().getBajaVinculoConDueño());
			animal.evaluarPosibleCambioDeDuenio(per);
		}
	}

}
