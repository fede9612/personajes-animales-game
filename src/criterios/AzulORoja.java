package criterios;

import animales.Animal;
import personaje.Personaje;
import pocimas.Pocima;

public class AzulORoja extends Criterio{

	@Override
	public void aplicarseConDuenio(Personaje per, Animal animal) {
		if(per.tienePocimaAzul()){
			aplicaAzulConDuenioYLaDesecha(per, animal);
		}else if(per.tienePocimaRoja()){
			aplicaRojaConDuenioYLaDescarta(per, animal);
		}
	}

	@Override
	public void aplicarseSinDuenio(Personaje per, Animal animal) {
		if(per.tienePocimaAzul()){
			aplicaAzulSinDuenioYLaDesecha(per, animal);
		}else if(per.tienePocimaRoja()){
			aplicaRojaConDuenioYLaDesecha(per, animal);
		}
	}


	private void aplicaRojaConDuenioYLaDesecha(Personaje per, Animal animal) {
		Pocima roja = per.getPocimaRoja();
		animal.bajarResistencia(roja.getBajaResistencia());
		per.quitarPocima(roja);
	}


	private void aplicaAzulSinDuenioYLaDesecha(Personaje per, Animal animal) {
		Pocima azul = per.getPocimaAzul();
		animal.bajarResistencia(azul.getBajaResistencia());
		per.quitarPocima(azul);
	}

	private void aplicaRojaConDuenioYLaDescarta(Personaje per, Animal animal) {
		Pocima roja = per.getPocimaRoja();
		animal.subirEnergia(roja.getAumentaEnergia());
		per.quitarPocima(roja);
	}
	
	private void aplicaAzulConDuenioYLaDesecha(Personaje per, Animal animal) {
		Pocima azul = per.getPocimaAzul();
		animal.subirResistencia(azul.getSubeResistencia());	
		per.quitarPocima(azul);
	}
}
