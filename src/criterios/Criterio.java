package criterios;

import animales.Animal;
import personaje.Personaje;

public abstract class Criterio {

	public abstract void aplicarseConDuenio(Personaje per, Animal animal);
	public abstract void aplicarseSinDuenio(Personaje per, Animal animal);
 
}
