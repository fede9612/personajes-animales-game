package pocimas;

public class DecoradorPocima {
	
	private Pocima pocima;
	
	public DecoradorPocima(Pocima pocima) {
		this.pocima = pocima;
	}
	
	public String getTipoDePocima(){
		return this.pocima.getClass().getSimpleName();
	}
	
}
