package pocimas;

public abstract class Pocima {

	public abstract int getBajaResistencia();

	public abstract int getSubeResistencia();

	public abstract int getBajaVinculoConDueño();

	public abstract int getBajaEnergiaAnimal();

	public abstract int getAumentaEnergia();
	
	public abstract String getNombre();

}
