package pocimas;

public class Amarilla extends Pocima{
	
	private int bajaEnergiaAnimal;
	private int bajaVinculoConDueño;
	
	public Amarilla(int bajaEnergiaAnimal, int bajavinculoConDueño) {
		this.bajaEnergiaAnimal = bajaEnergiaAnimal;
		this.bajaVinculoConDueño = bajavinculoConDueño;
	}
	
	@Override
	public int getBajaEnergiaAnimal() {
		return bajaEnergiaAnimal;
	}


	@Override
	public int getBajaVinculoConDueño() {
		return bajaVinculoConDueño;
	}

	public int getBajaResistencia() {
		return 0;
	}

	public int getSubeResistencia() {
		return 0;
	}

	@Override
	public int getAumentaEnergia() {
		return 0;
	}

	@Override
	public String getNombre() {
		return "Amarilla";
	}
	
	
	
}
