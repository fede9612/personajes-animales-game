package pocimas;

public class Azul extends Pocima{
	
	int subeResistencia;
	int bajaResistencia;
	
	public Azul(int sube, int baja){
		this.subeResistencia = sube;
		this.bajaResistencia = baja;
	}

	@Override
	public int getSubeResistencia() {
		return this.subeResistencia;
	}

	@Override
	public int getBajaResistencia() {
		return this.bajaResistencia;
	}

	public int getBajaVinculoConDueño() {
		return 0;
	}

	public int getBajaEnergiaAnimal() {
		return 0;
	}

	@Override
	public int getAumentaEnergia() {
		return 0;
	}

	@Override
	public String getNombre() {
		return "Azul";
	}
	
}
