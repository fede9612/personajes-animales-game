package pocimas;

public class Roja extends Pocima{
	
	private int aumentaEnergia;
	private int bajaResistencia;
	
	public Roja(int aumentaEnergia, int bajaResistencia) {
		this.aumentaEnergia = aumentaEnergia;
		this.bajaResistencia = bajaResistencia;
	}

	@Override
	public int getAumentaEnergia() {
		return this.aumentaEnergia;
	}
	
	@Override
	public int getBajaResistencia() {
		return this.bajaResistencia;
	}
	
	@Override
	public int getSubeResistencia() {
		return 0;
	}

	@Override
	public int getBajaVinculoConDueño() {
		return 0;
	}

	@Override
	public int getBajaEnergiaAnimal() {
		return 0;
	}

	@Override
	public String getNombre() {
		return "Roja";
	}


	

}
