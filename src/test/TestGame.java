package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import animales.Animal;
import animales.Buey;
import animales.Perro;
import castillos.Agricola;
import castillos.Castillo;
import castillos.Maligno;
import castillos.Resistente;
import criterios.AzulORoja;
import criterios.Criterio;
import criterios.VaritaOAmarilla;
import game.Game;
import personaje.Personaje;
import pocimas.Amarilla;
import varita.VaritaVerdadera;

public class TestGame {
	
	private Game game = Game.unico();
	private Personaje alberto;
	private Personaje jose;
	private Animal buey;
	private Animal perro;
	private Castillo castilloAgricola;
	private Criterio criterioAzulORojo;
	private Criterio criterioVaritaOAmarilla;
	private Castillo castilloResistente;
	private int cantPocimas;
	
	@Before
	public void setUp(){
		alberto = new Personaje();
		jose = new Personaje();
		buey = new Buey(2, 2);
		perro = new Perro(3, 6);
		castilloResistente = new Resistente();
		castilloAgricola = new Agricola(3, 1);
		criterioAzulORojo = new AzulORoja();
		criterioVaritaOAmarilla = new VaritaOAmarilla();
	}
	
	@Test
	public void encuentroDeAnimalPersonajeAplicaAzulSubeResistenciaPorqueEsSuDuenioYDesechaLaPocimaUsada(){
		alberto.agregarAnimal(buey);
		castilloAgricola.recargar();
		castilloAgricola.pasaPorElCastillo(alberto);
		cantPocimas = alberto.getPocimas().size();
		game.cambiarCriterio(criterioAzulORojo);
		game.encuentro(alberto, buey);
		
		assertEquals(5, buey.getResistencia());
		assertEquals((cantPocimas - 1), alberto.getPocimas().size());
	}
	
	@Test
	public void encuentroDeAnimalPersonajeAplicaAzulBajaResistenciaPorqueNoEsSuDuenioYDesechaLaPocimaUsada(){
		castilloAgricola.recargar();
		castilloAgricola.pasaPorElCastillo(alberto);
		cantPocimas = alberto.getPocimas().size();
		game.cambiarCriterio(criterioAzulORojo);
		game.encuentro(alberto, buey);
		
		assertEquals(1, buey.getResistencia());
		assertEquals((cantPocimas - 1), alberto.getPocimas().size());
	}
	
	@Test
	public void encuentroDeAnimalPersonajeAplicaRojoPorqueNoTieneAzulYEsSuDuenioYDesechaLaPocimaUsada(){
		alberto.agregarAnimal(buey);
		castilloResistente.recargar();
		castilloResistente.pasaPorElCastillo(alberto);
		cantPocimas = alberto.getPocimas().size();
		game.cambiarCriterio(criterioAzulORojo);
		game.encuentro(alberto, buey);
		
		assertEquals(6, buey.getEnergia());
		assertEquals((cantPocimas - 1), alberto.getPocimas().size());
	}
	
	@Test
	public void aplicarVaritaAUnAnimalPropioYSubeEn6ElVinculo(){
		alberto.agregarAnimal(buey);
		alberto.aumentarSabiduria(18);
		alberto.nuevaVarita(new VaritaVerdadera(alberto));
		alberto.usarVarita(buey);
		
		assertEquals(6, buey.getFuerzaVinculo());
	}
	
	
	@Test
	public void seAplicaVaritaAUnAnimalPropioYSubeEn6ElVinculoSegunCriterioVaritaOAmarilla(){
		alberto.agregarAnimal(buey);
		alberto.aumentarSabiduria(18);
		alberto.nuevaVarita(new VaritaVerdadera(alberto));
		game.cambiarCriterio(criterioVaritaOAmarilla);
		game.encuentro(alberto, buey);
		
		assertEquals(6,  buey.getFuerzaVinculo());
	}
	
	@Test
	public void seAplicaAmarillaAUnAnimalAjenoSegunCriterioVaritaOAmarilla(){
		buey.subirEnergia(10);
		alberto.agregarPocima(new Amarilla(10, 3));
		alberto.nuevaVarita(new VaritaVerdadera(alberto));
		game.cambiarCriterio(criterioVaritaOAmarilla);
		game.encuentro(alberto, buey);
		
		assertEquals(2, buey.getEnergia());
	}
	
	@Test 
	public void seAplicaAmarillaAUnAnimalAjenoSegunCriterioVaritaOAmarillaYLaEnergiaNoBajaDeCero(){
		alberto.agregarPocima(new Amarilla(10, 3));
		alberto.nuevaVarita(new VaritaVerdadera(alberto));
		game.cambiarCriterio(criterioVaritaOAmarilla);
		game.encuentro(alberto, buey);
		
		assertEquals(0, buey.getEnergia());
	}
	
	@Test
	public void fortalezaDeUnJugadorEs3PorqueSuBueyTiene2DeEnergiaYElTiene1DeSabiduria(){
		alberto.aumentarSabiduria(1);
		alberto.agregarAnimal(buey);
		
		assertEquals(3, alberto.fortaleza());
	}
	
	@Test
	public void antiguedadDelJuegoEs3PorqueHay3VisitantesEnLosCastillosDelGame(){
		game.getCastillos().clear();
		Castillo agricola = new Agricola(4, 2);
		Castillo maligno = new Maligno();
		Castillo resistente = new Resistente();
		agricola.pasaPorElCastillo(alberto);
		maligno.pasaPorElCastillo(alberto);
		resistente.pasaPorElCastillo(alberto);
		
		assertEquals(3, game.antiguedad());
	}
	
	@Test
	public void bueyDeAlbertoPasaASerLibre(){
		agregarAnimalAAlbertoYConfiguraPersonaje(buey, 30);
		jose.agregarPocima(new Amarilla(10, 3));
		jose.agregarPocima(new Amarilla(10, 3));
		jose.agregarPocima(new Amarilla(10, 3));
		jose.agregarPocima(new Amarilla(10, 3));
		
		game.cambiarCriterio(criterioVaritaOAmarilla);
		game.encuentro(jose, buey);
		game.encuentro(jose, buey);
		game.encuentro(jose, buey);
		game.encuentro(jose, buey);
		
		assertTrue(buey.esLibre());
		assertTrue(jose.tienePocimaAmarilla());
		assertEquals(0, buey.getFuerzaVinculo());
	}

	private void agregarAnimalAAlbertoYConfiguraPersonaje(Animal animal, int aumentaSabiduria) {
		alberto.agregarAnimal(animal);
		alberto.aumentarSabiduria(aumentaSabiduria);
		alberto.nuevaVarita(new VaritaVerdadera(alberto));
		alberto.usarVarita(animal);
	}
	
	@Test
	public void perroDeAlbertoPasaASerLibre(){
		agregarAnimalAAlbertoYConfiguraPersonaje(perro, 9);
		
		jose.agregarPocima(new Amarilla(10, 3));
		game.cambiarCriterio(criterioVaritaOAmarilla);
		game.encuentro(jose, perro);
		assertTrue(perro.esLibre());
	}
	
	@Test
	public void albertoTiene15DeSabiduria(){
		alberto.aumentarSabiduria(15);
		assertEquals(15, alberto.getSabiduria());
	}
	
	@Test
	public void albertoSeLeDa50SabiduriaPeroTieneMaximo30(){
		alberto.aumentarSabiduria(50);
		assertEquals(30, alberto.getSabiduria());
	}
	
	@Test
	public void perroLeAgrega1DeSabiduriaAAlbertoPorqueEsSuDuenio(){
		perro.subirFuerzaVinculo(5);
		perro.evaluarPosibleCambioDeDuenio(alberto);
		
		assertEquals(1, alberto.getSabiduria());
	}
	
	@Test
	public void perroLeResta1DeSabiduriaAAlbertoPorqueDejaDeSerSuDuenio(){
		perro.subirFuerzaVinculo(5);
		perro.evaluarPosibleCambioDeDuenio(alberto);
		perro.bajaVinculo(5);
		perro.evaluarPosibleCambioDeDuenio(alberto);
		
		assertEquals(0, alberto.getSabiduria());
	}
}
