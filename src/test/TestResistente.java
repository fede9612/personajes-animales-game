package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import animales.Buey;
import animales.DecoradorAnimal;
import animales.Perro;
import castillos.Castillo;
import castillos.Resistente;
import game.Game;
import personaje.Personaje;
import pocimas.DecoradorPocima;
import pocimas.Roja;

public class TestResistente {
	
	private Castillo castilloResistente;
	private Personaje alberto;

	@Before
	public void setUp(){
		castilloResistente = new Resistente();
		alberto = new Personaje();
		Game.unico().getAnimalesSueltos().clear();
	}
	
	@Test 
	public void seRecargaUnCastilloResistente(){
		castilloResistente.recargar();
		assertEquals(6, castilloResistente.getUnidades());
	}
	
	
	@Test
	public void resistenteAumentaLaSabiduriaEn2AlPersonaje(){
		castilloResistente.modificarSabiduria(alberto);
		
		assertEquals(2, alberto.getSabiduria());
	}
	
	
// ----------------------------------------------------- * -----------------------------------------------
// Acá uso un decorador para comparar que la pocima creada es azul, no ensucio el modelo y lo hago
// con un decorador	
	@Test
	public void resistenteCreaPocimaRojaYSeLaDaAlPersonaje(){
		castilloResistente.recargar();
		castilloResistente.crearPocimaPara(alberto);
		DecoradorPocima decoradorRoja = new DecoradorPocima(new Roja(5, 3));
		DecoradorPocima decoradorRojaAlberto = new DecoradorPocima(alberto.getPocimas().get(0));
		
		
		assertEquals(decoradorRoja.getTipoDePocima(), decoradorRojaAlberto.getTipoDePocima());
	}
	
	@Test 
	public void resistenteCreaBueyesPorqueElNumeroDeVisitantesEsParYSeAgregaAlJuego(){
		castilloResistente.crearAnimal();
		DecoradorAnimal decoradorBuey = new DecoradorAnimal(new Buey(6, 4));
		DecoradorAnimal decoradorBueyJuego = new DecoradorAnimal(Game.unico().getAnimalesSueltos().get(0));
		
		assertEquals(decoradorBuey.getTipoDeAnimal(), decoradorBueyJuego.getTipoDeAnimal());
	}
	
	@Test 
	public void resistenteCreaPerroPorqueElNumeroDeVisitantesEsImparYSeAgregaAlJuego(){
		castilloResistente.agregarVisitante(alberto);
		castilloResistente.crearAnimal();
		DecoradorAnimal decoradorPerro = new DecoradorAnimal(new Perro(2, 2));
		DecoradorAnimal decoradorPerroJuego = new DecoradorAnimal(Game.unico().getAnimalesSueltos().get(0));
		
		assertEquals(decoradorPerro.getTipoDeAnimal(), decoradorPerroJuego.getTipoDeAnimal());
	}
	
	@Test
	public void pasoDeUnPersonajePorElCastilloMaligno(){
		alberto.getPocimas().clear();
		castilloResistente.recargar();
		castilloResistente.pasaPorElCastillo(alberto);
		
		assertEquals(1, alberto.getPocimas().size());
		assertEquals(2, alberto.getSabiduria());
		assertEquals(1, castilloResistente.getCantidadDeVisitantes());
	}
	
}
