package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import castillos.Agricola;
import castillos.Castillo;
import castillos.Maligno;
import game.Game;

public class TestVarios {
	
	@Before
	public void setUp(){
		Game.unico().getCastillos().clear();
	}
	
	@Test
	public void seAgregaUnCastilloAgricolaYSeAgregaAlGame(){
		Castillo agricola = new Agricola(4, 2);
		
		assertEquals(1, Game.unico().getCastillos().size());
	}
	
	@Test
	public void seAgregaUnCastilloMalignoYSeAgregaAlGame(){
		Castillo maligno = new Maligno();
		
		assertEquals(1, Game.unico().getCastillos().size());
	}
	
}
