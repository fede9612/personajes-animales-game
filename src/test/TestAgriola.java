package test;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import animales.Buey;
import animales.DecoradorAnimal;
import castillos.Agricola;
import castillos.Castillo;
import game.Game;
import personaje.Personaje;
import pocimas.Azul;
import pocimas.DecoradorPocima;
import pocimas.Pocima;

public class TestAgriola {
	
	private Personaje alberto;
	private Castillo castilloAgricola;
	
	@Before
	public void setUp(){
		alberto = new Personaje();
		castilloAgricola = new Agricola(5, 2);
	}
	
	@Test 
	public void seRecargaUnCastilloAgricola(){
		castilloAgricola.recargar();
		assertEquals(6, castilloAgricola.getUnidades());
	}
	
	
	@Test
	public void registrarElPasoDeUnPersonajePorUnCastilloAgricolaSoloUnaVez(){
		castilloAgricola.pasaPorElCastillo(alberto);
		castilloAgricola.pasaPorElCastillo(alberto);
		Set<Personaje> viajantes = new HashSet<>();
		viajantes.add(alberto);
		
		assertEquals(viajantes, castilloAgricola.getVisitantes());		
	}
	
	
	@Test
	public void castilloCreaPosimaYSeLaDaAPersonaje(){
		castilloAgricola.recargar();
		castilloAgricola.crearPocimaPara(alberto);
		Pocima pocimaDeAlberto = alberto.getPocimas().get(0);
		assertEquals(5, pocimaDeAlberto.getSubeResistencia());
	}
	
	@Test
	public void aumentaLaSabiduriaEn2DeUnPersonaje(){
		castilloAgricola.recargar();
		castilloAgricola.modificarSabiduria(alberto);
		
		assertEquals(2, alberto.getSabiduria());
	}
	
	@Test
	public void aumentaLosAtributosDeLaPocimaCreadaPorqueElPersonajeEsSabio(){
		castilloAgricola.recargar();
		alberto.aumentarSabiduria(22);
		castilloAgricola.crearPocimaPara(alberto);
		Pocima pocimaDeAlberto = alberto.getPocimas().get(0);
		
		assertEquals(5, pocimaDeAlberto.getBajaResistencia());
		assertEquals(8, pocimaDeAlberto.getSubeResistencia());
	}
	
	
// ----------------------------------------------------- * -----------------------------------------------
// Acá uso un decorador para comparar que la pocima creada es azul, no ensucio el modelo y lo hago
// con un decorador
	
	@Test
	public void crearPocimaAzulParaUnPersonaje(){
		castilloAgricola.recargar();
		castilloAgricola.pasaPorElCastillo(alberto);
		DecoradorPocima decoradorAzul = new DecoradorPocima(new Azul(5, 2));
		DecoradorPocima decoradorAzulalberto = new DecoradorPocima(alberto.getPocimas().get(0));
		
		assertEquals(decoradorAzul.getTipoDePocima(), decoradorAzulalberto.getTipoDePocima());
	}
	
	
// ----------------------------------------------------- * -----------------------------------------------
// Acá uso un decorador para comparar que el animal creado sea un buey, no ensucio el modelo y lo hago
// con un decorador	
	@Test
	public void castilloCreaUnBueyCon5DeResistencia5DeEnergiaYLoAgregaAlJuego(){
		castilloAgricola.crearAnimal();
		DecoradorAnimal decoradorBuey = new DecoradorAnimal(new Buey(2, 3));
		DecoradorAnimal decoradorBueyJuego = new DecoradorAnimal(Game.unico().getAnimalesSueltos().get(0));
		
		assertEquals(5, Game.unico().getAnimalesSueltos().get(0).getResistencia());
		assertEquals(5, Game.unico().getAnimalesSueltos().get(0).getEnergia());
		assertEquals(decoradorBuey.getTipoDeAnimal(), decoradorBueyJuego.getTipoDeAnimal());
		
	}	
	
	
	@Test
	public void pasoDeUnPersonajePorElCastilloAgricola(){
		alberto.getPocimas().clear();
		castilloAgricola.recargar();
		castilloAgricola.pasaPorElCastillo(alberto);
		
		assertEquals(1, alberto.getPocimas().size());
		assertEquals(2, alberto.getSabiduria());
		assertEquals(1, castilloAgricola.getCantidadDeVisitantes());
	}
}
