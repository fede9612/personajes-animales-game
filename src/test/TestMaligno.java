package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import animales.DecoradorAnimal;
import animales.Hiena;
import castillos.Castillo;
import castillos.Maligno;
import game.Game;
import personaje.Personaje;
import pocimas.Amarilla;
import pocimas.DecoradorPocima;

public class TestMaligno {
	
	
	private Castillo castilloMaligno;
	private Personaje alberto;

	@Before
	public void setUp(){
		castilloMaligno = new Maligno();
		alberto = new Personaje();
	}
	
	
	@Test
	public void malignoDisminuyeEn5LaSabiduriaDelPersonaje(){
		alberto.aumentarSabiduria(10);
		castilloMaligno.modificarSabiduria(alberto);
		
		assertEquals(5, alberto.getSabiduria());
	}
	
	@Test 
	public void seRecargaUnCastilloMaligno(){
		castilloMaligno.recargar();
		assertEquals(6, castilloMaligno.getUnidades());
	}
	
	
	
	@Test
	public void malignoRecuerdaSoloUnaVezElPersonajeQuePasPorEl(){
		castilloMaligno.recordarVisitante(alberto);
		castilloMaligno.recordarVisitante(alberto);
		
		assertEquals(1, castilloMaligno.getVisitantes().size());
	}

// ----------------------------------------------------- * -----------------------------------------------
// Acá uso un decorador para comparar que la pocima creada es azul, no ensucio el modelo y lo hago
// con un decorador	
	@Test
	public void malignoCreaPocimaAmarillaYSeLaDaAlPersonaje(){
		castilloMaligno.recargar();
		castilloMaligno.crearPocimaPara(alberto);
		DecoradorPocima decoradorAmarilla = new DecoradorPocima(new Amarilla(5, 2));
		DecoradorPocima decoradorAmarillaAlberto = new DecoradorPocima(alberto.getPocimas().get(0));
		assertEquals(decoradorAmarilla.getTipoDePocima(), decoradorAmarillaAlberto.getTipoDePocima());
	}

	
	
// ----------------------------------------------------- * -----------------------------------------------
// Acá uso un decorador para comparar que el animal creado sea un buey, no ensucio el modelo y lo hago
// con un decorador
	@Test 
	public void malignoCreaUnAnimalHienaYLoAgregaAlJuego(){
		castilloMaligno.crearAnimal();
		DecoradorAnimal decoradorHiena= new DecoradorAnimal(new Hiena(2, 5));
		DecoradorAnimal decoradorHienaJuego = new DecoradorAnimal(Game.unico().getAnimalesSueltos().get(0));
		
		assertEquals(decoradorHiena.getTipoDeAnimal(), decoradorHienaJuego.getTipoDeAnimal());
	}
	
	@Test
	public void pasoDeUnPersonajePorElCastilloMaligno(){
		alberto.getPocimas().clear();
		castilloMaligno.recargar();
		castilloMaligno.pasaPorElCastillo(alberto);
		
		assertEquals(1, alberto.getPocimas().size());
		assertEquals(0, alberto.getSabiduria());
		assertEquals(1, castilloMaligno.getCantidadDeVisitantes());
	}
}
