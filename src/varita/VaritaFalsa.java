package varita;

import animales.Animal;
import personaje.Personaje;

public class VaritaFalsa extends Varita{

	public VaritaFalsa(Personaje duenio) {
		super(duenio);
	}

	@Override
	public void aplicarse(Animal animal) {
		// Acá no hace nada porque es una Varita falsa sin poder
	}

}
