package varita;

import animales.Animal;
import personaje.Personaje;

public abstract class Varita {
	
	private Personaje duenio;
	
	public Varita(Personaje duenio) {
		this.duenio = duenio;
	}
	
	public abstract void aplicarse(Animal animal);

	public Personaje getDuenio() {
		return duenio;
	}
	
}
