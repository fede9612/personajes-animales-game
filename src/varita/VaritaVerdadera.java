package varita;

import animales.Animal;
import personaje.Personaje;

public class VaritaVerdadera extends Varita{

	public VaritaVerdadera(Personaje duenio) {
		super(duenio);
	}

	@Override
	public void aplicarse(Animal animal) {
		animal.subirFuerzaVinculo(this.getDuenio().getSabiduria() / 3);
	}

}
