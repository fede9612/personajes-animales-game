package castillos;

import animales.Hiena;
import game.Game;
import personaje.Personaje;
import pocimas.Amarilla;

public class Maligno extends Castillo{

	public Maligno() {
		super();
	}
	
	@Override
	public void crearAnimal() {
		Game.unico().agregarAnimal(new Hiena(2, 6));
	}

	@Override
	public void crearPocimaPara(Personaje per) {
		if(this.puedoCrearPocima()){
			per.agregarPocima(new Amarilla(10, 3));
		}
	}

	@Override
	public void modificarSabiduria(Personaje per) {
		per.disminuirSabiduria(5);
	}

}
