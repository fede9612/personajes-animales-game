package castillos;

import java.util.HashSet;
import java.util.Set;

import game.Game;
import personaje.Personaje;

public abstract class Castillo {
	
	private int unidades = 0;
	private Set<Personaje> visitantes = new HashSet<>();
	
	public Castillo() {
		Game.unico().agregarCastillo(this);
	}
	
	public void recargar(){
		this.unidades += 6;
	}
	
	public abstract void crearAnimal();
	
	public void pasaPorElCastillo(Personaje per){
		this.crearPocimaPara(per);
		this.modificarSabiduria(per);
		this.recordarVisitante(per);
	}
	
	public abstract void crearPocimaPara(Personaje per);
	public abstract void modificarSabiduria(Personaje per);
	
	public int getUnidades() {
		return unidades;
	}

	public void agregarVisitante(Personaje per){
		this.visitantes.add(per);
	}
	
	public Set<Personaje> getVisitantes() {
		return visitantes;
	}

	public boolean puedoCrearPocima() {
		return (this.getUnidades() >= 1) ? true : false;
	}

	public void recordarVisitante(Personaje per) {
		this.agregarVisitante(per);
	}
	
	public int getCantidadDeVisitantes(){
		return this.visitantes.size();
	}
}
