package castillos;

import animales.Buey;
import game.Game;
import personaje.Personaje;
import pocimas.Azul;

public class Agricola extends Castillo{

	private int sube;
	private int baja;

	public Agricola(int sube, int baja){
		super();
		this.sube = sube;
		this.baja = baja;
	}	
	
	@Override
	public void crearPocimaPara(Personaje per) {
		cambiarValorAPocimaSiEsSabio(per);
		if(this.puedoCrearPocima()){
			per.agregarPocima(new Azul(this.getSube(), this.getBaja()));
		}
	}


	private void cambiarValorAPocimaSiEsSabio(Personaje per) {
		if(per.getSabiduria() > 20){
			this.sube += 3;
			this.baja += 3;
		}
	}

	@Override
	public void modificarSabiduria(Personaje per) {
			per.aumentarSabiduria(2);
	}

	@Override
	public void recordarVisitante(Personaje per) {
		this.agregarVisitante(per);
	}

	@Override
	public void crearAnimal() {
		Game.unico().agregarAnimal(new Buey(5, 5));
	}

	public int getSube() {
		return sube;
	}

	public int getBaja() {
		return baja;
	}

}
