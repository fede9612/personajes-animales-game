package castillos;

import animales.Buey;
import animales.Perro;
import game.Game;
import personaje.Personaje;
import pocimas.Roja;

public class Resistente extends Castillo{
	
	public Resistente() {
		super();
	}
	
	@Override
	public void crearAnimal() {
		if(cantDeVisitantesEspar()){
			Game.unico().agregarAnimal(new Buey(12, 5));
		}else{
			Game.unico().agregarAnimal(
					new Perro(this.getVisitantes().size(),
							this.getVisitantes().size())
					);			
		}
	}

	private boolean cantDeVisitantesEspar() {
		return this.getVisitantes().size() %2 == 0;
	}

	@Override
	public void crearPocimaPara(Personaje per) {
		if(this.puedoCrearPocima()){
			per.agregarPocima(new Roja(4, 3));
		}
	}

	@Override
	public void modificarSabiduria(Personaje per) {
		per.aumentarSabiduria(2);
	}

}
